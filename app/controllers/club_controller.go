package controllers

import (
	"github.com/gofiber/fiber/v2"
	"go-rest/app/models/nsi"
)

// GetClubs func gets all exists clubs.
// @Description Получить все клубы.
// @Summary получить все клубы
// @Tags Club
// @Accept json
// @Produce json
// @Success 200 {array} nsi.Club
// @Router /v1/club [get]
func GetClubs(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	return GetModels(c, "Clubs", db.ClubQueries)
}

// GetClub func gets team by given ID or 404 error.
// @Description Get club by given ID.
// @Summary get club by given ID
// @Tags Club
// @Accept json
// @Produce json
// @Param id path string true "Club ID"
// @Success 200 {object} nsi.Club
// @Router /v1/club/{id} [get]
func GetClub(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	team := GetModel(c, "Club", db.ClubQueries)
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"team":  team,
	})
}

// CreateClub func for creates a new team.
// @Description Create a new club.
// @Summary create a new club
// @Tags Club
// @Accept json
// @Produce json
// @Param club body nsi.Club true "club"
// @Success 200 {object} nsi.Club
// @Security ApiKeyAuth
// @Router /v1/club [post]
func CreateClub(c *fiber.Ctx) error {

	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	// Create new Team struct
	model := &nsi.Club{}
	return CreateModel(c, &model, "Club", db.ClubQueries)
}

// UpdateClub func for updates club by given ID.
// @Description Update club.
// @Summary update club
// @Tags Club
// @Accept json
// @Produce json
// @Param team body nsi.Club true "club"
// @Success 201 {string} status "ok"
// @Security ApiKeyAuth
// @Router /v1/club [put]
func UpdateClub(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	model := &nsi.Club{}
	return UpdateModel(c, &model, "Club", db.ClubQueries)
}

// DeleteClub func for deletes club by given ID.
// @Description Delete club by given ID.
// @Summary delete club by given ID
// @Tags Club
// @Accept json
// @Produce json
// @Param  id body int true "Club ID" Format(int64)
// @Success 204 {string}  status "ok"
// @Security ApiKeyAuth
// @Router /v1/club [delete]
func DeleteClub(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	model := &nsi.Club{}
	return DeleteModel(c, &model, "Club", db.ClubQueries)
}
