package controllers

import (
	"go-rest/pkg/utils"
	"go-rest/platform/database"
	"reflect"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
)

func GetDb() (*database.Queries, error) {
	return database.OpenDBConnection()
}

func GetModels(c *fiber.Ctx, alias string, db interface{}) error {
	models := reflect.ValueOf(db).MethodByName("Get" + alias).Call([]reflect.Value{})[0].Interface()
	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"teams": models,
	})
}

func GetModel(c *fiber.Ctx, alias string, db interface{}) interface{} {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}
	objects := make(map[reflect.Type]interface{})
	objects[reflect.TypeOf(reflect.Int)] = id
	aliasMethod := "Get" + alias
	method := reflect.ValueOf(db).MethodByName(aliasMethod)
	in := make([]reflect.Value, method.Type().NumIn())
	in[0] = reflect.ValueOf(id)
	model := method.Call(in)[0].Interface()
	return model
}

func CreateModel(c *fiber.Ctx, dest interface{}, alias string, db interface{}) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := utils.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Set expiration time from JWT data of current book.
	expires := claims.Expires

	// Checking, if now time greather than expiration from JWT.
	if now > expires {
		// Return status 401 and unauthorized error message.
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "unauthorized, check expiration time of your token",
		})
	}

	// Create new Team struct
	model := &dest

	//Check, if received JSON data is valid.
	if err := c.BodyParser(model); err != nil {
		// Return status 400 and error message.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	objects := make(map[reflect.Type]interface{})
	objects[reflect.TypeOf(model)] = model

	aliasMethod := "Create" + alias
	method := reflect.ValueOf(db).MethodByName(aliasMethod)
	in := make([]reflect.Value, method.Type().NumIn())
	in[0] = reflect.ValueOf(*model)
	method.Call(in)[0].Interface()

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"model": model,
	})
}

func UpdateModel(c *fiber.Ctx, dest interface{}, alias string, db interface{}) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := utils.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Set expiration time from JWT data of current book.
	expires := claims.Expires

	// Checking, if now time greather than expiration from JWT.
	if now > expires {
		// Return status 401 and unauthorized error message.
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "unauthorized, check expiration time of your token",
		})
	}

	// Create new Book struct
	model := &dest

	// Check, if received JSON data is valid.
	if err := c.BodyParser(model); err != nil {
		// Return status 400 and error message.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	aliasMethod := "Update" + alias
	method := reflect.ValueOf(db).MethodByName(aliasMethod)
	in := make([]reflect.Value, method.Type().NumIn())
	in[0] = reflect.ValueOf(*model)
	method.Call(in)[0].Interface()

	// Return status 201.
	return c.SendStatus(fiber.StatusCreated)
}

func DeleteModel(c *fiber.Ctx, dest interface{}, alias string, db interface{}) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := utils.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Set expiration time from JWT data of current book.
	expires := claims.Expires

	// Checking, if now time greather than expiration from JWT.
	if now > expires {
		// Return status 401 and unauthorized error message.
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "unauthorized, check expiration time of your token",
		})
	}

	model := &dest

	// Check, if received JSON data is valid.
	if err := c.BodyParser(model); err != nil {
		// Return status 400 and error message.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	aliasMethod := "Delete" + alias
	method := reflect.ValueOf(db).MethodByName(aliasMethod)
	in := make([]reflect.Value, method.Type().NumIn())
	in[0] = reflect.ValueOf(*model)
	method.Call(in)[0].Interface()

	// Return status 204 no content.
	return c.SendStatus(fiber.StatusNoContent)
}
