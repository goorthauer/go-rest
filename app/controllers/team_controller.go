package controllers

import (
	"github.com/gofiber/fiber/v2"
	"go-rest/app/models/nsi"
)

// GetTeams func gets all exists teams.
// @Description Получить все команды.
// @Summary получить все команды
// @Tags Team
// @Accept json
// @Produce json
// @Success 200 {array} nsi.Team
// @Router /v1/team [get]
func GetTeams(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	return GetModels(c, "Teams", db.TeamQueries)
}

// GetTeam func gets team by given ID or 404 error.
// @Description Get team by given ID.
// @Summary get team by given ID
// @Tags Team
// @Accept json
// @Produce json
// @Param id path string true "Team ID"
// @Success 200 {object} nsi.Team
// @Router /v1/team/{id} [get]
func GetTeam(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	team := GetModel(c, "Team", db)
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"team":  team,
	})
}

// CreateTeam func for creates a new team.
// @Description Create a new team.
// @Summary create a new team
// @Tags Team
// @Accept json
// @Produce json
// @Param team body nsi.TeamPost true "team"
// @Success 200 {object} nsi.Team
// @Security ApiKeyAuth
// @Router /v1/team [post]
func CreateTeam(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	// Create new Team struct
	team := &nsi.Team{}
	return CreateModel(c, &team, "Team", db)
}

// UpdateTeam func for updates team by given ID.
// @Description Update team.
// @Summary update team
// @Tags Team
// @Accept json
// @Produce json
// @Param team body nsi.TeamPut true "team"
// @Success 201 {string} status "ok"
// @Security ApiKeyAuth
// @Router /v1/team [put]
func UpdateTeam(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	team := &nsi.Team{}
	return UpdateModel(c, &team, "Team", db)
}

// DeleteTeam func for deletes team by given ID.
// @Description Delete team by given ID.
// @Summary delete team by given ID
// @Tags Team
// @Accept json
// @Produce json
// @Param  id body int true "Team ID" Format(int64)
// @Success 204 {string}  status "ok"
// @Security ApiKeyAuth
// @Router /v1/team [delete]
func DeleteTeam(c *fiber.Ctx) error {
	db, err := GetDb()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "error",
		})
	}
	team := &nsi.Team{}
	return DeleteModel(c, &team, "Team", db)
}
