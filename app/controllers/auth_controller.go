package controllers

import (
	"github.com/gofiber/fiber/v2"
	"go-rest/app/models/public"
	"go-rest/pkg/utils"
)

// Login method for login by username and password.
// @Description login user.
// @Summary Login user
// @Tags User
// @Accept json
// @Produce json
// @Param team body public.UserLogin true "user"
// @Success 200 {string} status "ok"
// @Router /v1/login [post]
func Login(c *fiber.Ctx) error {
	// Generate a new Access token.
	userStruct := &public.UserStruct{}
	err := c.BodyParser(userStruct)
	if err != nil {
		// Return status 500 and token generation error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}
	if !userStruct.FindUser() {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   "Invalid username or password",
		})
	}
	token, err := utils.GenerateNewAccessToken(userStruct)
	if err != nil {
		// Return status 500 and token generation error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"error":        false,
		"msg":          nil,
		"access_token": token,
	})
}
