package queries

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"go-rest/app/models/nsi"
)

// TeamQueries struct for queries from Team model.
type TeamQueries struct {
	*sqlx.DB
	team nsi.Team
}

func (q *TeamQueries) CoreQuery() sq.SelectBuilder {
	var selectTeam string
	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	getSqlField(&q.team, &selectTeam, "team")
	tableName := q.team.TableName()
	active := psql.Select(selectTeam).From(tableName).LeftJoin(q.team.GetClubJoin()).OrderBy("team.id")
	return active
}

// GetTeams method for getting all teams.
func (q *TeamQueries) GetTeams() ([]nsi.Team, error) {
	var teams []nsi.Team
	active := q.CoreQuery()
	sql, _, err := active.ToSql()
	if err != nil {
		return teams, err
	}
	err = q.Select(&teams, sql)
	if err != nil {
		return teams, err
	}
	return teams, nil
}

// GetTeam method for getting one team by given ID.
func (q *TeamQueries) GetTeam(id int) (nsi.Team, error) {
	// Define team variable.
	team := nsi.Team{}
	active := q.CoreQuery().Where("team.id IN (?)", id)
	query, args, err := active.ToSql()
	if err != nil {
		return team, err
	}
	// Send query to database.
	err = q.Get(&team, query, args...)
	if err != nil {
		// Return empty object and error.
		return team, err
	}
	//if !team.ClubID.Valid {0
	//	team.Club = nil
	//}
	// Return query result.
	return team, nil
}

//CreateTeam method for creating team by given Team object.
func (q *TeamQueries) CreateTeam(team **nsi.Team) error {
	query := BasicCreate(q.DB, q.team.TableName(), sq.Eq{"club_id": &(*team).Club.ID, "name": &(*team).Name, "gender": &(*team).Gender})
	err := query.QueryRow().Scan(&(*team).ID)
	if err != nil {
		// Return only error.
		return err
	}

	// This query returns nothing.
	return nil
}

// UpdateTeam method for updating team by given Team object.
func (q *TeamQueries) UpdateTeam(team **nsi.Team) error {

	query, args, err := BasicUpdate(q.DB, q.team.TableName(), &(*team).ID, sq.Eq{"club_id": &(*team).Club.ID, "name": &(*team).Name, "gender": &(*team).Gender})
	if err != nil {
		// Return only error.
		return err
	}
	_, err = q.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

// DeleteTeam method for delete team by given ID.
func (q *TeamQueries) DeleteTeam(team **nsi.Team) error {

	query, args, err := BasicDelete(q.DB, q.team.TableName(), (*team).ID)
	_, err = q.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}
