package queries

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"reflect"
)

// BasicQueries struct for queries from all model.
type BasicQueries struct {
	*sqlx.DB
}

func setStringField(fieldTag string, structString *string, alias string, substr bool) *string {
	if fieldTag != "" {
		if *structString != "" {
			*structString += ", "
		}

		*structString += alias + "." + fieldTag
		if substr {
			*structString += " as " + "\"" + alias + "." + fieldTag + "\""
		}
	}
	return structString
}

func getStructFields(t reflect.Type, column *string, alias string, subStr bool) *string {
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		fieldTag := field.Tag.Get("db")
		if field.Type.Kind() == reflect.Struct {
			getStructFields(field.Type, column, fieldTag, true)
			continue
		}
		column = setStringField(fieldTag, column, alias, subStr)
	}
	return column
}

func getSqlField(dest interface{}, selectColumn *string, alias string) {
	getStructFields(reflect.TypeOf(dest).Elem(), selectColumn, alias, false)
	return
}

// BasicDelete method for delete team by given ID.
func BasicDelete(db *sqlx.DB, table string, id int) (string, []interface{}, error) {

	dbCache := sq.NewStmtCache(db)
	return sq.Delete(table).
		Where(sq.Eq{"id": id}).
		PlaceholderFormat(sq.Dollar).
		RunWith(dbCache).ToSql()
}

// BasicUpdate method for update
func BasicUpdate(db *sqlx.DB, table string, id *int, mapField sq.Eq) (string, []interface{}, error) {

	dbCache := sq.NewStmtCache(db)
	return sq.Update(table).
		RunWith(dbCache).
		SetMap(mapField).
		PlaceholderFormat(sq.Dollar).
		Where(sq.Eq{"id": id}).
		ToSql()
}

// BasicCreate method for create
func BasicCreate(db *sqlx.DB, table string, mapField sq.Eq) sq.InsertBuilder {
	dbCache := sq.NewStmtCache(db)
	return sq.Insert(table).
		SetMap(mapField).
		Suffix("RETURNING \"id\"").
		RunWith(dbCache).
		PlaceholderFormat(sq.Dollar)
}
