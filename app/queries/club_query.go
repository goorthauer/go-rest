package queries

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"go-rest/app/models/nsi"
)

// ClubQueries struct for queries from Team model.
type ClubQueries struct {
	*sqlx.DB
	model nsi.Club
}

func (q *ClubQueries) CoreQuery() sq.SelectBuilder {
	var selectModel string
	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	getSqlField(&q.model, &selectModel, "club")
	tableName := q.model.TableName()
	active := psql.Select(selectModel).From(tableName).OrderBy("id")
	return active
}

// GetClubs method for getting all teams.
func (q *ClubQueries) GetClubs() ([]nsi.Club, error) {
	var models []nsi.Club
	active := q.CoreQuery()
	sql, _, err := active.ToSql()
	if err != nil {
		return models, err
	}
	err = q.Select(&models, sql)
	if err != nil {
		return models, err
	}
	return models, nil
}

// GetClub method for getting one team by given ID.
func (q *ClubQueries) GetClub(id int) (nsi.Club, error) {
	// Define team variable.
	club := nsi.Club{}
	active := q.CoreQuery().Where("club.id IN (?)", id)
	query, args, err := active.ToSql()
	if err != nil {
		return club, err
	}
	// Send query to database.
	err = q.Get(&club, query, args...)
	if err != nil {
		// Return empty object and error.
		return club, err
	}
	//if !team.ClubID.Valid {0
	//	team.Club = nil
	//}
	// Return query result.
	return club, nil
}

//CreateClub method for creating team by given Team object.
func (q *ClubQueries) CreateClub(model **nsi.Club) error {
	query := BasicCreate(q.DB, q.model.TableName(), sq.Eq{"name": &(*model).Name})
	err := query.QueryRow().Scan(&(*model).ID)
	if err != nil {
		// Return only error.
		return err
	}

	// This query returns nothing.
	return nil
}

// UpdateClub method for updating team by given Team object.
func (q *ClubQueries) UpdateClub(model **nsi.Club) error {

	query, args, err := BasicUpdate(q.DB, q.model.TableName(), (*model).ID, sq.Eq{"name": &(*model).Name})
	if err != nil {
		// Return only error.
		return err
	}
	_, err = q.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

// DeleteClub method for delete team by given ID.
func (q *ClubQueries) DeleteClub(model **nsi.Team) error {

	query, args, err := BasicDelete(q.DB, q.model.TableName(), (*model).ID)
	_, err = q.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}
