package nsi

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
)

// Fighter struct to describe book object.
type Fighter struct {
	ID     int           `db:"id" json:"id" validate:"required,int"`
	Name   string        `db:"name" json:"name" validate:"required,string"`
	TeamID sql.NullInt64 `db:"team_id" json:"team_id"`
	ClubID sql.NullInt64 `db:"club_id" json:"club_id"`
	Height int           `db:"height" json:"height"`
	Weight int           `db:"weight" json:"weight"`
	Team   Team          `db:"team" json:"team"`
	Club   Club          `db:"club" json:"club"`
}

// Value make the Fighter struct implement the driver.Valuer interface.
// This method simply returns the JSON-encoded representation of the struct.
func (b Fighter) Value() (driver.Value, error) {
	return json.Marshal(b)
}
