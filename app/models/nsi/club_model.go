package nsi

import (
	"database/sql/driver"
	"encoding/json"
)

// Club struct to describe club object.
type Club struct {
	ID   *int    `db:"id" json:"id"`
	Name *string `db:"name" json:"name"`
}

type ClubID struct {
	ID *int `json:"id"`
}

func (cl Club) TableName() string {
	return "nsi.club"
}

// Value make the Club struct implement the driver.Valuer interface.
// This method simply returns the JSON-encoded representation of the struct.
func (cl Club) Value() (driver.Value, error) {
	val, err := json.Marshal(cl)
	return string(val), err
}
