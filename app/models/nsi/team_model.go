package nsi

import (
	"database/sql/driver"
	"encoding/json"
)

// Team struct to describe team object.
type Team struct {
	ID     int     `db:"id" json:"id" example:"48"`
	Name   *string `db:"name" json:"name"`
	Gender *int    `db:"gender" json:"gender"`
	Club   Club    `db:"club" json:"club"`
}

type TeamPost struct {
	Club   ClubID  `json:"club"`
	Gender *int    `db:"gender" json:"gender" example:"1"`
	Name   *string `db:"name" json:"name" example:"Тестовая команда"`
}
type TeamPut struct {
	ID     int     `db:"id" json:"id" example:"48"`
	Name   *string `db:"name" json:"name" example:"ВОТ ЭТО ДА"`
	Gender *int    `db:"gender" json:"gender" example:"0"`
	Club   ClubID  `db:"club" json:"club"`
}

// Value make the Team struct implement the driver.Valuer interface.
// This method simply returns the JSON-encoded representation of the struct.
func (t Team) Value() (driver.Value, error) {
	val, err := json.Marshal(t)
	return string(val), err
}

func (Team) TableName() string {
	return "nsi.team"
}

func (t Team) GetClubJoin() string {
	return "nsi.club on club.id = team.club_id"
}
