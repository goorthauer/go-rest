package public

type UserStruct struct {
	Id       int    `db:"id" json:"id"`
	Login    string `db:"login" json:"login"`
	Password string `db:"password" json:"password"`
}

type UserLogin struct {
	Login    string `db:"login" json:"login" example:"John"`
	Password string `db:"password" json:"password" example:"666"`
}

func FillUser() []UserStruct {
	users := []UserStruct{
		{
			Id:       1,
			Login:    "John",
			Password: "666",
		},
		{
			Id:       2,
			Login:    "Silva",
			Password: "555",
		},
		{
			Id:       3,
			Login:    "Bogdan",
			Password: "test",
		},
	}
	return users
}

func (user *UserStruct) FindUser() bool {
	users := FillUser()
	result := false
	for _, product := range users {
		if product.Login == user.Login && product.Password == user.Password {
			result = true
			break
		}
	}
	return result
}
