package main

import (
	"github.com/gofiber/fiber/v2"
	"go-rest/pkg/configs"
	"go-rest/pkg/middleware"
	"go-rest/pkg/routes"
	"go-rest/pkg/utils"

	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	_ "go-rest/docs"                      // load API Docs files (Swagger)
)

// @title API
// @version 1.0.0
// @description Описание автосгенерированного АПИ.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @BasePath /api
func main() {
	// Define Fiber config.
	config := configs.FiberConfig()

	// Define a new Fiber app with config.
	app := fiber.New(config)

	// Middlewares.
	middleware.FiberMiddleware(app) // Register Fiber's middleware for app.

	// Routes.
	routes.SwaggerRoute(app)  // Register a route for API Docs (Swagger).
	routes.PublicRoutes(app)  // Register a public routes for app.
	routes.PrivateRoutes(app) // Register a private routes for app.
	routes.NotFoundRoute(app) // Register route for 404 Error.

	// Start server (with graceful shutdown).
	utils.StartServer(app)
}
