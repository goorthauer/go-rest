package routes

import (
	"github.com/gofiber/fiber/v2"
	"go-rest/app/controllers"
)

// PublicRoutes func for describe group of public routes.
func PublicRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Routes for GET method:
	route.Get("/token/new", controllers.GetNewAccessToken) // create a new access tokens

	route.Post("/login", controllers.Login) // create a new access tokens

	route.Get("/team", controllers.GetTeams)    // get list of all teams
	route.Get("/team/:id", controllers.GetTeam) // get one team by ID

	route.Get("/club", controllers.GetClubs)    // get list of all clubs
	route.Get("/club/:id", controllers.GetClub) // get one club by ID

}
