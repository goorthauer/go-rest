package routes

import (
	"github.com/gofiber/fiber/v2"
	"go-rest/app/controllers"
	"go-rest/pkg/middleware"
)

// PrivateRoutes func for describe group of private routes.
func PrivateRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Routes for POST method:
	route.Post("/team", middleware.JWTProtected(), controllers.CreateTeam) // create a new team
	route.Post("/club", middleware.JWTProtected(), controllers.CreateClub) // create a new club

	// Routes for PUT method:
	route.Put("/team", middleware.JWTProtected(), controllers.UpdateTeam) // update one team by ID
	route.Put("/club", middleware.JWTProtected(), controllers.UpdateClub) // update one club by ID

	// Routes for DELETE method:
	route.Delete("/team", middleware.JWTProtected(), controllers.DeleteTeam) // delete one team by ID
	route.Delete("/club", middleware.JWTProtected(), controllers.DeleteClub) // delete one club by ID
}
