module go-rest

go 1.16

require (
	github.com/Masterminds/squirrel v1.5.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/arsmn/fiber-swagger/v2 v2.6.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.5.0
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/gofiber/jwt/v2 v2.2.1
	github.com/google/uuid v1.2.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/swag v1.7.0
)
