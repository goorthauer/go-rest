package database

import "go-rest/app/queries"

// Queries struct for collect all app queries.
type Queries struct {
	*queries.TeamQueries // load queries from Book model
	*queries.ClubQueries
}

// OpenDBConnection func for opening database connection.
func OpenDBConnection() (*Queries, error) {
	// Define a new PostgreSQL connection.
	db, err := PostgreSQLConnection()
	if err != nil {
		return nil, err
	}

	return &Queries{
		// Set queries from models:
		TeamQueries: &queries.TeamQueries{DB: db}, // from Team model
		ClubQueries: &queries.ClubQueries{DB: db}, // from Club model
	}, nil
}
